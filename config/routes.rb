Rails.application.routes.draw do
  root to: 'dashboard/positions#index'

  namespace :dashboard do
    resources :positions, only: [:index]
  end

  namespace :api do
    namespace :common_api do
      resources :positions, only: [:create, :update, :destroy]
    end

    resources :positions, only: [:update]
  end
end
