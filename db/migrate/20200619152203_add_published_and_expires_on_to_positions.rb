class AddPublishedAndExpiresOnToPositions < ActiveRecord::Migration[5.2]
  def change
    add_column :positions, :published, :boolean, default: true
    add_column :positions, :expires_on, :date
  end
end
