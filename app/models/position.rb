class Position < ApplicationRecord
  extend Positions::Scopes

  def unpublish!
    self.published = false
    self.save!
  end
end
