module Positions
  # separate class for creating positions in the DB
  class Creator
    def initialize(attributes)
      @attributes = attributes
    end

    def call
      Position.create!(attributes)
    end

    private

    attr_reader :attributes
  end
end
