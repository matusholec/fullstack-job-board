module Positions
  # separate class for updating positions in the DB
  class Updater
    def initialize(position, attributes)
      @position = position
      @attributes = attributes
    end

    def call
      position.update_attributes(attributes)
    end

    private

    attr_reader :position, :attributes
  end
end
