module CommonApi
  class Client
    include HTTParty

    def self.update_position(position)
      # this is just a quick hack to make the code work in dev / better solution would be to have
      # some kind of sandbox env for development
      return true if Rails.env.development?
      response = HTTParty.patch(
        position.update_uri,
        {headers: auth_header, body: ::CommonAPI::Mapper.outgoing_patch_request(position)}
      )
      return false unless response.code == 204
      true
    end

    def self.auth_header
      { Authorization: "Bearer #{token}" }
    end

    def self.token
      JWT.encode( { sub: ENV.fetch('JWT_PUBLIC_KEY') }, ENV.fetch('JWT_SECRET_KEY'), 'HS256')
    end

    private_class_method :auth_header, :token
  end
end
