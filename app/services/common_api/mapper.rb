module CommonAPI
  class Mapper
    def self.incoming_request(attributes)
      {
        reference: attributes.dig('UserArea', 'PostingID'),
        title: attributes['PositionTitle'],
        company_name: attributes['OrganizationName'],
        update_uri: attributes.dig('UserArea', 'PostingURI')
      }
    end

    def self.outgoing_patch_request(position)
      {
        "data": {
          "type": "postings",
          "attributes": {
            "PublicURI": position.update_uri,
            "ExpiresAt": position.expires_on&.noon.to_s
          }
        }
      }
    end
  end
end
