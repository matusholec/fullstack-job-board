# frozen_string_literal: true
module Dashboard
  class PositionsController < ApplicationController
    def index
      ::Dashboard::Positions::Index.new(self, params).call
    end
  end
end
