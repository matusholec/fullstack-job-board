module Dashboard
  module Positions
    class Index < ::ControllerAction
      def call
        expose(serialized_positions, '@positions')
      end

      private

      def serialized_positions
        Position.published.order(:created_at).map do |position|
          ::PositionSerializer.new(position).to_h
        end
      end
    end
  end
end
