module Api
  module Positions
    # controller action handling PATCH positions/update from internal client
    class Update < ControllerAction
      def call
        if position
          handle_position_update
        else
          render status: 500
        end
      end

      private

      def safe_params
        params.permit(:title, :company_name, :expires_on)
      end

      def position
        @position ||= Position.find_by(id: params[:id])
      end

      def update_position
        ::Positions::Updater.new(position, safe_params).call
      end

      def serialized_position
        ::PositionSerializer.new(position).to_h
      end

      # we want to update position internally only if external update passes as well
      def handle_position_update
        ActiveRecord::Base.transaction do
          if update_position && update_position_on_capi
            render json: {position: serialized_position}, status: :ok
          else
            # would be a good idea to ping error monitoring service with some error
            raise ActiveRecord::Rollback
            render status: 500
          end
        end
      end

      def update_position_on_capi
        ::CommonApi::Client.update_position(position)
      end
    end
  end
end
