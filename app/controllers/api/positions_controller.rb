# reusing JWT for internal API communication
require Rails.root.join('lib/auth/jwt')

module Api
  # Receives requests [create, update, delete] from internal API (react app)
  class PositionsController < ::Api::ApplicationController
    def update
      ::Api::Positions::Update.new(self, params).call
    end
  end
end
