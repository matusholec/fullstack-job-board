module Api
  module CommonApi
    module Positions
      # controller action positions/delete, we do not delete records as we might want to keep
      # the historical data, rather do a soft delete - unpublish
      class Destroy < ControllerAction
        def call
          position.unpublish!
          render head: :no_content, content_type: 'application/vnd.api+json'
        end

        private

        def position
          Position.find(params[:id])
        end
      end
    end
  end
end
