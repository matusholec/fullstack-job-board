module Api
  module CommonApi
    module Positions
      # controller action handling positions/create
      class Create < ControllerAction
        def call
          render json: {
            data: { type: 'position-opening', links: { self: controller.api_common_api_position_url(position) } }
          }, status: 201, content_type: 'application/vnd.api+json'
        end

        private

        def position
          ::Positions::Creator.new(normalised_position_attributes).call
        end

        def normalised_position_attributes
          ::CommonAPI::Mapper.incoming_request(attributes)
        end
      end
    end
  end
end
