module Api
  module CommonApi
    module Positions
      # controller action handling PATCH positions/update
      class Update < ControllerAction
        def call
          if position && update_position
            render status: :no_content, content_type: 'application/vnd.api+json'
          else
            render status: :not_found, content_type: 'application/vnd.api+json'
          end
        end

        private

        def position
          @position ||= Position.find_by(id: params[:id])
        end

        def normalised_position_attributes
          @normalised_position_attributes ||= ::CommonAPI::Mapper.incoming_request(attributes).compact!
        end

        def payload_reference_matches_url_id?
          normalised_position_attributes[:reference] == position.reference
        end

        def update_position
          return false unless payload_reference_matches_url_id?
          ::Positions::Updater.new(position, normalised_position_attributes).call
        end
      end
    end
  end
end
