# frozen_string_literal: true

require Rails.root.join('lib/auth/jwt')

module Api
  module CommonApi
    # Receives requests [create, update, delete] from the Common API.
    class PositionsController < ::Api::CommonApi::ApplicationController
      def create
        ::Api::CommonApi::Positions::Create.new(self, params).call
      end

      def update
        ::Api::CommonApi::Positions::Update.new(self, params).call
      end

      def destroy
        ::Api::CommonApi::Positions::Destroy.new(self, params).call
      end
    end
  end
end
