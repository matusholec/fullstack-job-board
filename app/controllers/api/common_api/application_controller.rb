module Api
  module CommonApi
    class ApplicationController < ActionController::API
      before_action :authenticate

      def attributes
        params.require(:data)['attributes']
      end

      def authenticate
        head 401 unless Auth::Jwt.build(auth_token).valid?
      end

      def auth_token
        request.headers['Authorization'].to_s.split(' ', 2).last
      end
    end
  end
end
