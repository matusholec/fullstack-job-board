module Api
  class ApplicationController < ActionController::API
    before_action :authenticate

    def authenticate
      head 401 unless Auth::Jwt.build(auth_token).valid?
    end

    def auth_token
      request.headers['Authorization'].to_s.split(' ', 2).last
    end
  end
end
