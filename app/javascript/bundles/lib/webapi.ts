import {
  patchJSON,
} from './requests'

import { Position } from '../types'

export const updatePosition = (position: Position, params: object) => {
  return patchJSON(`/api/positions/${position.id}`, params)
}
