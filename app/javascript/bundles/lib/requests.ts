/* eslint-disable no-unused-vars */
import axios from 'axios';

const cacheBuster = `?date=${new Date().toISOString()}`;

// be a lot cooler to put the token into some secrets.yml
const token = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJzb21lcHVibGljdG9rZW4ifQ.StVrGwp1Qu3CD6Yn340ZB2crEoZEQvf3XACOeyEsFTw'

const authHeader = { Authorization: "Bearer " + token }

axios.interceptors.response.use((response) => response, (error) => {
  if (error.status === 422) {
    return Promise.resolve(error);
  }
  return Promise.reject(error);
})

export function getJSON(path: string, params = {}) {
  return axios.get(path + cacheBuster, {params})
}

export function putJSON(path: string, params = {}) {
  return axios.put(path, params, { headers: authHeader })
}

export function patchJSON(path: string, params = {}) {
  return axios.patch(path, params, { headers: authHeader })
}

export function postJSON(path: string, params = {}) {
  return axios.post(path, params, { headers: authHeader })
}
