import { Position } from '../../types'

export const updatePosition = (position: Position, params: any) => {
  return Promise.resolve({ data: {position: {...position, ...params}} })
}
