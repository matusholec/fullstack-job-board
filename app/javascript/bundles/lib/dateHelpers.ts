//needs format mm.dd.yyyy
export const stringToDate: (s: string) => Date = (str: string) => {
  return new Date(parseInt(str.slice(6,10)), parseInt(str.slice(3,5)) - 1, parseInt(str.slice(0,2)))
}
