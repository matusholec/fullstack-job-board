import React from 'react'

import { createUseStyles } from 'react-jss'

export const CustomInputComponent = (props: any) => {
  const classes = useStyles()
  return (
    <input className={classes.input} type="text" {...props} />
  )
}

const useStyles = createUseStyles(() => ({
  input: {
    width: '100%',
  },
}))
