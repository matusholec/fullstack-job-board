import React from 'react'

import DatePicker from 'react-datepicker'
import { createUseStyles } from 'react-jss'

import "react-datepicker/dist/react-datepicker.css";

export interface CustomDatePickerComponentProps {
  setStartDate: (d: Date) => void
  setFieldValue: (name: string, value: any) => void
  startDate: Date | null
}

export const CustomDatePickerComponent = ( { setStartDate, setFieldValue, startDate }: CustomDatePickerComponentProps ) => {
  const classes = useStyles()

  return (
    <DatePicker
      dateFormat="dd.MM.yyyy"
      className={classes.input}
      selected={startDate}
      onChange={(date: Date) =>{
        setStartDate(date)
        setFieldValue('expires_on', date.toDateString())
      }}
    />
  )
}

const useStyles = createUseStyles(() => ({
  input: {
    width: '100%',
  },
}))
