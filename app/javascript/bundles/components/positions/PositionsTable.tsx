import React from 'react'

import { Container, Row, Col } from 'react-bootstrap'
import { createUseStyles } from 'react-jss'
import { Position } from '../../types'
import { PositionRow } from './PositionRow'
import { ExpiresOnFilter } from './filters/ExpiresOnFilter'

export interface PositionsTableProps {
  positions: Position[]
  handleEditPosition: (p: Position) => void
  filterByExpiresOn: (expiresOn: string) => void
  cancelFilter: () => void
}

export const PositionsTable = ( { positions, handleEditPosition, filterByExpiresOn, cancelFilter }: PositionsTableProps ) => {
  const classes = useStyles()

  return (
    <div className={classes.container}>
      <h1 className={classes.heading}>Positions Dashboard</h1>
      <Container className={classes.positionsTable}>
        <ExpiresOnFilter onFilter={filterByExpiresOn} onCancelFilter={cancelFilter}/>
        <Row className={classes.headerRow}>
          <Col className={classes.headerCol}>Title</Col>
          <Col className={classes.headerCol}>Company Name</Col>
          <Col className={classes.headerCol}>Expiration Date</Col>
          <Col className={classes.lastHeaderCol}>Action</Col>
        </Row>
          {positions.map((position, index) =>
            <PositionRow key={index} position={position} handleEditPosition={handleEditPosition} />
          )}
      </Container>
    </div>
  )
}

const useStyles = createUseStyles(() => ({
  heading: {
    textAlign: 'center',
  },
  positionsTable: {
    marginTop: '50px',
  },
  container: {
    marginTop: '20px',
    marginLeft: '30px',
    marginRight: '30px',
  },
  headerRow: {
    fontWeight: 'bold',
    alignItems: 'center',
    marginTop: '25px',
  },
  headerCol: {
    borderLeft: '1px solid',
    borderTop: '1px solid',
    borderBottom: '1px solid',
  },
  lastHeaderCol: {
    border: '1px solid',
  },
}))
