import React, { useState } from 'react'

import { Button, Col, Row } from 'react-bootstrap'
import { Form, Field, FormikErrors } from 'formik'
import { createUseStyles } from 'react-jss'

import { Position } from '../../types'
import { CustomInputComponent, CustomDatePickerComponent } from '../form'
import { stringToDate } from '../../lib/dateHelpers'

export interface PositionRowEditFormProps {
  position: Position
  isSubmitting: boolean
  onCancelClick: () => void
  setFieldValue: (name: string, value: any ) => void
  errors: FormikErrors<Position>
}

export const PositionRowEditForm = ({ position, onCancelClick, isSubmitting, setFieldValue, errors }: PositionRowEditFormProps) => {
  const classes = useStyles()
  const [startDate, setStartDate] = useState(position.expires_on ? stringToDate(position.expires_on) : null)
  const anyValidationErrors = Object.keys(errors).length > 0

  return (
    <Form>
      <Row className={classes.bodyRow}>
        <Col className={errors.title ? classes.bodyColError : classes.bodyCol}>
          <Field as={CustomInputComponent} name="title" />
        </Col>
        <Col className={errors.company_name ? classes.bodyColError : classes.bodyCol}>
          <Field as={CustomInputComponent} name="company_name" />
        </Col>
        <Col className={classes.bodyCol}>
          <CustomDatePickerComponent setStartDate={setStartDate} startDate={startDate} setFieldValue={setFieldValue} />
        </Col>
        <Col className={classes.submitBtnCol}>
          { <Button
              variant='success'
              type='submit'
              className={classes.submitBtn}
              disabled={isSubmitting || anyValidationErrors}
            >
              Submit
            </Button> }
          { <Button
              variant='secondary'
              className={classes.cancelBtn}
              onClick={() => onCancelClick()}
              disabled={isSubmitting}
            >
              Cancel
            </Button> }
        </Col>
      </Row>
    </Form>
  )
}

const useStyles = createUseStyles(() => ({
  bodyRow: {
    border: '1px solid #dee2e6',
    backgroundColor: 'lightYellow'
  },
  bodyCol: {
    borderRight: '1px solid #dee2e6',
    paddingTop: '8px',
    paddingBottom: '8px',
  },
  submitBtnCol: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  submitBtn: {
    marginRight: '5px',
    marginLeft: '-7px',
  },
  cancelBtn: {
    marginLeft: '5px',
  },
  input: {
    width: '100%',
  },
  bodyColError: {
    border: '2px solid red',
    paddingTop: '8px',
    paddingBottom: '8px',
  },
}))
