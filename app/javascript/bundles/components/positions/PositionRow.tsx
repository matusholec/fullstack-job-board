import React, { useState } from 'react'

import { Button, Col, Row } from 'react-bootstrap'
import { createUseStyles } from 'react-jss'

import { Position } from '../../types'
import { PositionEditContainer } from '../../containers/PositionEditContainer'

interface PositionRowProps {
  position: Position
  handleEditPosition: (p: Position) => void
}

export const PositionRow = ( { position, handleEditPosition}: PositionRowProps ) => {
  const [isEditing, setIsEditing] = useState(false)
  const classes = useStyles()

  if (isEditing) {
    return (
      <PositionEditContainer
        position={position}
        onSubmitClick={() => setIsEditing(false)}
        handleEditPosition={handleEditPosition}
      />
    )
  }
  return (
    <Row className={classes.bodyRow}>
      <Col className={classes.bodyCol}>{position.title}</Col>
      <Col className={classes.bodyCol}>{position.company_name}</Col>
      <Col className={classes.bodyCol}>{position.expires_on}</Col>
      <Col className={classes.editBtnCol}>{
        <Button variant='link' block onClick={ () => setIsEditing(true) } >Edit</Button>
      }</Col>
    </Row>
  )
}

const useStyles = createUseStyles(() => ({
  bodyRow: {
    border: '1px solid #dee2e6'
  },
  bodyCol: {
    borderRight: '1px solid #dee2e6',
    paddingTop: '8px',
    paddingBottom: '8px',
  },
  editBtnCol: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
  },
}))
