import React, { useState } from 'react'

import { Button, Col, Row } from 'react-bootstrap'
import DatePicker from 'react-datepicker'
import dayjs from 'dayjs'

export interface ExpiresOnFilterProps {
  onFilter: (date: string) => void
  onCancelFilter: () => void
}

export const ExpiresOnFilter = ( { onFilter, onCancelFilter }: ExpiresOnFilterProps ) => {
  const [selectedDate, setSelectedDate] = useState(null as Date | null)

  return (
    <div>
      <Row>
        <Col>Expires on: <DatePicker
          dateFormat="dd.MM.yyyy"
          selected={selectedDate}
          onChange={(date: Date) => {
            setSelectedDate(date)
            onFilter(dayjs(date).format('DD.MM.YYYY'))
          }
          } />
        </Col>
      </Row>
      <hr/>
      <Row>
        <Col xs='3'>
          <Button variant='secondary'
            onClick={() => {
              setSelectedDate(null)
              onCancelFilter()
            }
          }>
            Cancel Filter
          </Button>
        </Col>
      </Row>
    </div>
  )
}
