export interface Position {
  id: string
  title: string
  company_name: string
  expires_on?: string
}
