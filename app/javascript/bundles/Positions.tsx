// pass-through wrapping component so that we can use react hooks in the PositionsContainer

import React, { useState, useEffect } from 'react'

import { PositionsContainer } from './containers/PositionsContainer'
import { Position } from './types'

interface PositionsProps {
  positions: Position[]
}

export function Positions( { positions }: PositionsProps ) {

  return (
    <PositionsContainer positions={positions} />
  )
}
