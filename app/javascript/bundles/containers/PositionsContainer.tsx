import React, { useState, useEffect } from 'react'

import { PositionsTable } from '../components/positions/PositionsTable'
import { Position } from '../types'

export interface PositionsContainerProps {
  positions: Position[]
}

export function PositionsContainer( { positions }: PositionsContainerProps ) {
  const [positionsList, setPositionsList] = useState([{}] as Position[])
  const [filterExpiresOn, setFilterExpiresOn] = useState(null as String | null)

  useEffect(() => {
    setPositionsList(positions);
  }, [positions]);

  const handleEditPosition = (position: Position) => {
    setPositionsList(positionsList.map(p => p.id === position.id ? Object.assign({}, position) : p))
  }

  const filterByExpiresOn = (expiresOn: string) => {
    setFilterExpiresOn(expiresOn)
  }

  const cancelFilter = () => {
    setFilterExpiresOn(null)
  }

  const filteredPositions = filterExpiresOn ?
    positionsList.filter((position: Position) => position.expires_on === filterExpiresOn) : positionsList


  return (
    <PositionsTable
      positions={filteredPositions}
      handleEditPosition={handleEditPosition}
      filterByExpiresOn={filterByExpiresOn}
      cancelFilter={cancelFilter}
    />
  )
}
