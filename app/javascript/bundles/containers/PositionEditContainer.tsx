import React from 'react'

import { Formik, FormikErrors } from 'formik'

import { Position } from '../types'
import { PositionRowEditForm } from '../components/positions/PositionRowEditForm'
import { updatePosition } from '../lib/webapi'

interface PositionEditContainerProps {
  position: Position
  onSubmitClick: () => void
  handleEditPosition: (p: Position) => void
}

export const PositionEditContainer = ({ position, onSubmitClick, handleEditPosition }: PositionEditContainerProps) => {
  return (
    <div>
      <Formik
        initialValues={{ title: position.title, company_name: position.company_name, expires_on: position.expires_on }}
        validate={values => {
          const errors: FormikErrors<Position> = {}
          if (!values.title) {
            errors.title = 'Required'
          }
          if (!values.company_name) {
            errors.company_name = 'Required'
          }
          return errors
        }}
        onSubmit={(values, { setSubmitting }) => {
          updatePosition(position, values).then(
            ({ data: { position } }) => {
              setSubmitting(false)
              onSubmitClick()
              handleEditPosition(position)
            }
          ).catch((error) => {
            console.log('ERROR: ', error)
            setSubmitting(false)
            onSubmitClick()
            alert('There was a server error')
          })
        }}
      >
        {({ isSubmitting, setFieldValue, errors }) => (
          <PositionRowEditForm
            position={position}
            setFieldValue={setFieldValue}
            isSubmitting={isSubmitting}
            onCancelClick={onSubmitClick}
            errors={errors}
          />
        )}
      </Formik>
    </div>
  )
}
