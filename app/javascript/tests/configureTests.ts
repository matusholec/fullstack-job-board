import Enzyme from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'

import { Position } from '../bundles/types'

Enzyme.configure({ adapter: new Adapter() })

jest.mock('react-jss', () => ({
  createUseStyles: (() => () => ({})),
}))

jest.mock('../bundles/lib/webapi', () => jest.requireActual('../bundles/lib/__mocks__/webapi'))
