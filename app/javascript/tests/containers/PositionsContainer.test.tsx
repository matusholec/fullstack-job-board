import React from 'react'
import { act } from 'react-dom/test-utils'

import DatePicker from 'react-datepicker'
import { mount } from 'enzyme'

import { PositionsContainer } from '../../bundles/containers/PositionsContainer'
import { positions } from '../apiFixtures'
import { PositionsTable } from '../../bundles/components/positions/PositionsTable'
import { Position } from '../../bundles/types'

describe('<PositionsContainer />', () => {
  function mountComponent() {

    return mount(<PositionsContainer positions={positions} />)
  }

  it('filters by expires on and click on cancel dismisses the filter', () => {
    const wrapper = mountComponent()
    const pickerInput = wrapper.find(DatePicker)
    const positionsTableComponent = () => wrapper.find(PositionsTable)
    const cancelButton = findComponentByText(wrapper.find('Button'), 'Cancel Filter')

    expect(positionsTableComponent().prop('positions')).toHaveLength(4)
    act(() => {
      pickerInput.instance().props.onChange(new Date(2020, 9, 21, 12))
    })
    wrapper.update()
    expect(positionsTableComponent().prop('positions')).toHaveLength(1)
    act(() => {
      cancelButton.simulate('click')
    })
    wrapper.update()
    expect(positionsTableComponent().prop('positions')).toHaveLength(4)
  })

  it('merges edited position into positions array', async() => {
    const wrapper = mountComponent()
    const positionsTableComponent = () => wrapper.find(PositionsTable)
    const editButton = findComponentByText(wrapper.find('Button'), 'Edit')
    editButton.simulate('click')
    const titleInput = wrapper.find("input[name='title']")

    const newTitle = 'Totally new title of a position'
    titleInput.instance().value = newTitle

    await act(async() => {
      titleInput.simulate('change')
    })
    const form = wrapper.find("form")

    await act(async() => {
      await form.simulate('submit')
    })

    wrapper.update()
    expect(positionsTableComponent().prop('positions').map((position: Position) => position.title)).toContain(newTitle)
  })

})

const findComponentByText = (wrapper: any, text: string) =>
  wrapper.findWhere((node: any) => {return (node.type() && node.name() && node.text() === text)}).at(0)
