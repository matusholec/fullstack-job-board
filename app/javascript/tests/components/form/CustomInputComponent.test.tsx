import React from 'react'

import { shallow } from 'enzyme'

import { CustomInputComponent } from '../../../bundles/components/form/CustomInputComponent'

describe('<CustomInputComponent />', () => {
  function mountComponent() {

    return shallow(<CustomInputComponent />)
  }

  it('matches the snapshot', () => {
    const wrapper = mountComponent()

    expect(wrapper).toMatchSnapshot()
  })
})
