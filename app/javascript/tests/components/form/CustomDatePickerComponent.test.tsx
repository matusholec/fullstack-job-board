import React from 'react'

import { shallow } from 'enzyme'
import DatePicker from 'react-datepicker'

import {
  CustomDatePickerComponent,
  CustomDatePickerComponentProps
} from '../../../bundles/components/form/CustomDatePickerComponent'

describe('<CustomDatePickerComponent />', () => {
  const defaultProps = {
      setStartDate: () => {},
      setFieldValue: () => {},
      startDate: new Date(2020, 10, 10)
    }

  function mountComponent(props: CustomDatePickerComponentProps) {

    return shallow(<CustomDatePickerComponent {...props} />)
  }

  it('matches the snapshot', () => {
    const wrapper = mountComponent(defaultProps)

    expect(wrapper).toMatchSnapshot()
  })

  it('calls setStartDate and setFieldValue when onChange is invoked', () => {
    const setStartDateMock = jest.fn()
    const setFieldValueMock = jest.fn()
    const wrapper = mountComponent({...defaultProps, setStartDate: setStartDateMock, setFieldValue: setFieldValueMock})

    wrapper.find(DatePicker).simulate('change', new Date(2020, 10, 1, 12))
    expect(setStartDateMock).toBeCalled
    expect(setFieldValueMock).toBeCalled
  })

})
