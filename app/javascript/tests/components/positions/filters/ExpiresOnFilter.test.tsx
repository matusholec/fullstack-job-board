import React from 'react'

import DatePicker from 'react-datepicker'
import dayjs from 'dayjs'

import { shallow } from 'enzyme'

import {
  ExpiresOnFilter,
  ExpiresOnFilterProps }
from '../../../../bundles/components/positions/filters/ExpiresOnFilter'

describe('<ExpiresOnFilter />', () => {
  const defaultProps = {
    onFilter: () => {},
    onCancelFilter: () => {},
  }

  function mountComponent(props: ExpiresOnFilterProps) {

    return shallow(<ExpiresOnFilter {...props} />)
  }

  it('matches the snapshot', () => {
    const wrapper = mountComponent(defaultProps)

    expect(wrapper).toMatchSnapshot()
  })

  it('fires off onFilter after date is selected', () => {
    const onFilterMock = jest.fn()
    const wrapper = mountComponent({ ...defaultProps, onFilter: onFilterMock })

    wrapper.find(DatePicker).simulate('change', new Date(2020, 10, 1))
    expect(onFilterMock).toBeCalled()
  })

  it('displays the selected date and clears it after clicking Cancel', () => {
    const onCancelFilterMock = jest.fn()
    const wrapper = mountComponent({ ...defaultProps, onCancelFilter: onCancelFilterMock })

    wrapper.find(DatePicker).simulate('change', new Date(2020, 10, 1, 12))
    const displayedDate = () => wrapper.find(DatePicker).prop('selected')
    expect(dayjs(displayedDate()).format('DD.MM.YYYY')).toEqual('01.11.2020')
    wrapper.find('Button').simulate('click')
    expect(displayedDate()).toEqual(null)
    expect(onCancelFilterMock).toBeCalled
  })
})
