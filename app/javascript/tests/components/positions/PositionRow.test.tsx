import React from 'react'

import { shallow } from 'enzyme'

import { PositionRow } from '../../../bundles/components/positions/PositionRow'
import { PositionEditContainer } from '../../../bundles/containers/PositionEditContainer'
import { position } from '../../apiFixtures'

describe('<PositionRow />', () => {
  function mountComponent() {
    return shallow(<PositionRow handleEditPosition={() => null} position={position} />)
  }

  it('matches the snapshot', () => {
    const wrapper = mountComponent()

    expect(wrapper).toMatchSnapshot()
  })

  it('renders editable container when clicking Button', () => {
    const wrapper = mountComponent()

    wrapper.find('Button').prop('onClick')()
    expect(wrapper.find(PositionEditContainer)).toHaveLength(1)
  })

  it('does not render editable container when not clicking a Button', () => {
    const wrapper = mountComponent()

    expect(wrapper.find(PositionEditContainer)).toHaveLength(0)
  })
})
