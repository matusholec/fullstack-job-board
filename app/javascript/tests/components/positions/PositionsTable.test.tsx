import React from 'react'

import { shallow } from 'enzyme'

import { PositionsTable, PositionsTableProps } from '../../../bundles/components/positions/PositionsTable'
import { positions } from '../../apiFixtures'

describe('<PositionsTable />', () => {
  const defaultProps = {
    positions: positions,
    handleEditPosition: () => {},
    filterByExpiresOn: () => {},
    cancelFilter: () => {}
  }

  function mountComponent(props: PositionsTableProps) {

    return shallow(<PositionsTable {...defaultProps} {...props} />)
  }

  // not much else to test here as it's a pure display component that passes through the props to its children
  it('matches the snapshot', () => {
    const wrapper = mountComponent(defaultProps)

    expect(wrapper).toMatchSnapshot()
  })
})
