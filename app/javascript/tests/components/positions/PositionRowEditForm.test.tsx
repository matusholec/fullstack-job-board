import React from 'react'

import { shallow, ShallowWrapper } from 'enzyme'

import { PositionRowEditForm, PositionRowEditFormProps } from '../../../bundles/components/positions/PositionRowEditForm'
import { position } from '../../apiFixtures'

describe('<PositionRowEditForm />', () => {
  function mountComponent(props: PositionRowEditFormProps | {}) {
    const defaultProps = {
      position: position,
      setFieldValue: () => {},
      isSubmitting: false,
      onCancelClick: () => {},
      errors: {}
    }

    return shallow(<PositionRowEditForm {...defaultProps} {...props} />)
  }

  it('matches the snapshot', () => {
    const wrapper = mountComponent({})

    expect(wrapper).toMatchSnapshot()
  })

  it('calls cancel click callback', () => {
    const onCancelClickMock = jest.fn()
    const wrapper = mountComponent({onCancelClick: onCancelClickMock})

    getButtonByAttribute(wrapper, 'variant', 'secondary').simulate('click')
    expect(onCancelClickMock).toBeCalled()
  })

  it('properly disables submit button when isSubmitting is true', () => {
    const wrapper = mountComponent({isSubmitting: true})

    expect(getButtonByAttribute(wrapper, 'type', 'submit').prop('disabled')).toEqual(true)
  })

  it('properly disables submit button when there are errors', () => {
    const wrapper = mountComponent({errors: {title: 'required'}})

    expect(getButtonByAttribute(wrapper, 'type', 'submit').prop('disabled')).toEqual(true)
  })
})

function getButtonByAttribute(wrapper: any, attributeName: string, attributeValue: string) {
  return wrapper.find('Button').filterWhere((item: any) => item.prop(attributeName) === attributeValue)
}


