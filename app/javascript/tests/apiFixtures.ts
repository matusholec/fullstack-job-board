export const api = {

}

export const positions = [
  {
    id: 'f62dcf11-e7f8-45fd-98b9-971737dbbabd',
    title: 'title',
    company_name: 'recontextualize leading-edge users',
    expires_on: '21.10.2020',
  },
  {
    id: 'fdgasdf11-e7f8-45fd-98b9-971737dbbabd',
    title: 'other title',
    company_name: 'Aliquid minima necessitatibus et.',
    expires_on: '20.10.2020',
  },
  {
    id: 'sdguh76-e7f8-45fd-98b9-971737dbbabd',
    title: 'Ullam qui maxime assumenda.',
    company_name: 'recontextualize sexy ROI',
    expires_on: '20.10.2020',
  },
  {
    id: 'swfgrhg76-e7f8-45fd-98b9-971737dbbabd',
    title: 'Ullam qui maxime assumenda.',
    company_name: 'recontextualize sexy ROI',
    expires_on: '10.10.2020',
  },
]

export const position = positions[0]
