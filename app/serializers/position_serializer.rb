class PositionSerializer
  def initialize(position)
    @position = position
  end

  def to_h
    {
      id:           position.id,
      title:        position.title,
      company_name: position.company_name,
      expires_on:   position.expires_on.to_s,
    }
  end

  private

  attr_reader :position
end
