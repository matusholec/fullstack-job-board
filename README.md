# Fullstack job board

This is the list of implemented functionality from the task description:

* Process an incoming PUT request to /api/positions. Hint: You can use the reviews payload which is under /spect/support/fixtures.
* Process an incoming DELETE request to /api/positions.
* Set an expiration date for the positions and use it to filter them on the index view.
* Edit an existing open position and push the changes following the CAPI specification.
* Request the expiration of one of the current open positions - again, please follow the CAPI specification.

To run the app just install gems `bundle`, setup db `rails db:setup` and run the server `script/server` and visit [http://localhost:3000](http://localhost:3000/)

To run specs: `rspec spec`

In this readme I try to explain the way I thought about the implementation and why I have chosen the exact approach.

## Approach towards rails
Just to be super clear - I don't say that everything stated here is a must and everyone should follow this path. I just find this approach to be scalable for bigger codebases, easily understandable and also I am most efficient with this kind of approach.

### Controllers

The main thing that deviates from standards are controllers. Controllers in this example never contain any logic, all they do is create an instance of a class dedicated to handling that action and then call it. This is to move a bit closer to pragmatic separation of concerns and conform to single responsibility principle a bit more (to a reasonable extent).

### Models

Models never contain ActiveRecord callbacks with business logic as I tend to avoid them cause a lot of times they are confusing and not used right, and also might be a source of various bugs. Instead logic from this lays within service objects.

There is always one service object for creating a record, in our case `::Positions::Creator` - a simple class where all the logic around creating objects resides. The same goes for updating records etc.

ActiveRecord scopes for models are located in `/app/models/MODEL_NAME/scopes.rb` file and then extend the given model class. This is so we don't flood the model.rb files with a lot of class method for queries. More complicated queries should of course be implemented as query objects on `/app/queries` dir.

I tried to write specs for every class, one thing I did not do was making capybara work with `react_on_rails` gem.

### Serializers
There are separate serializers for every resource we pass to our API.

### Biggest considerations (or what made me think the most)
I was not sure how to properly structure the API part of the app in the most readable way. The reason is that there is external API for CAPI and also internal API communication with client react app.
What I did is that everything lies in the `app/controllers/api` directory, and common api code is namespaced in subsequent `common_api` directory.

### What would be next
I would probably break code for Common API and internal API into separate rails engines. Another thing would be to make capybara work with `react_on_rails` as I find acceptance tests to be extremely beneficial towards gaining trust in the codebase.

## Approach towards react
As mentioned before I have used `react_on_rails` gem.
The approach is very simple - smart components reside within `app/javascript/bundles/containers/` directory, and presentation components used for html output lie in `app/javascript/bundles/components`.
I followed basic principles of containers VS components.

### Containers
For this projects I have chosen to go with a simple internal state of the container and not using any kind of state management.

I can imagine that the project to be more useful there would have to be a lot more functionality and therefore a need for proper state management would arise. In this case I'd go with react's built-in `useReducer` hook though. I'd probably not want to use graphql for internal API as the common API interface is using REST-API so I would not want to mix it unless it's a requirement. In that case I wouldn't use `useReducer` at all and just rely on react-apollo client.

The implemented containers communicate with server using axios library.

### Components
Used simply for displaying HTML. For this project I used styled components approach with react-jss. Simply because it's the most recent approach I've been working with, css-modules is also very scalable and nicely modular approach.

For forms I am using [Formik library](https://jaredpalmer.com/formik/docs/overview) - very easy to work with, lightweight but still offering robust functionality.

### Typescript
I am a fan of typescript mostly for typing data from API, I have also found it very useful for preventing unwanted bugs. Combination of Typescript and Capybara is in my opinion enough for making sure that frontend behaves correctly (at least for this exercise).

### Jest tests
All tests reside in the `app/javascript/tests` folder. I prefer having test grouped under single directory rather than having them scattered around the codebase sitting with the files they are testing - probably just a habit from rails.
Tu run tests simply run `jest` in the root directory. There are unit tests as well as one bigger container test that is sort of testing the whole application functionality.
