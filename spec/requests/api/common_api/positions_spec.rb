# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'Positions', type: :request do
  let(:public_key) { ENV.fetch('JWT_PUBLIC_KEY') }
  let(:secret_key) { ENV.fetch('JWT_SECRET_KEY') }
  let(:token) do
    JWT.encode( { sub: public_key }, secret_key, 'HS256')
  end
  let(:headers) do
    {
      'Content-Type' => 'application/vdn.api+json',
      Authorization: "Bearer #{token}"
    }
  end

  describe 'POST /positions' do
    subject(:post_request) do
      post api_common_api_positions_path, params: data, headers: headers
    end

    let(:data) { File.read('spec/fixtures/publication.json') }
    let(:attributes) { JSON.parse(data)['data']['attributes'] }

    it 'creates a new position' do
      expect { post_request }.to change { Position.count }.from(0).to(1)
    end

    it 'responds with a created status response' do
      post_request

      expect(response).to have_http_status(201)
    end

    it 'stores the reference, title, company name and update_uri' do
      post_request
      new_position = Position.take

      expect(new_position.reference).to eq(attributes.dig('UserArea', 'PostingID'))
      expect(new_position.title).to eq(attributes['PositionTitle'])
      expect(new_position.company_name).to eq(attributes['OrganizationName'])
      expect(new_position.update_uri).to eq(attributes.dig('UserArea', 'PostingURI'))
    end

    context 'when the Auth token is invalid' do
      let(:token) do
        JWT.encode(
          { sub: SecureRandom.uuid },
          'some wrong secret key',
          'HS256'
        )
      end

      it 'responds with an unauthorized response' do
        post_request

        expect(response).to have_http_status(401)
      end

      it 'does not create a position' do
        expect { post_request }.to_not(change { Position.count })
      end
    end
  end

  describe 'PATCH /positions/:id' do
    let(:data) { File.read('spec/fixtures/revision.json') }
    let(:attributes) { JSON.parse(data)['data']['attributes'] }

    subject(:patch_request) do
      patch api_common_api_position_url(position.id), params: data, headers: headers
    end

    context 'with data matching referenced position' do
      let!(:position) { Fabricate(:position, reference: attributes.dig('UserArea', 'PostingID')) }

      it 'updates position attributes with attributes from payload' do
        patch_request
        position.reload
        expect(position.title).to eq(attributes['PositionTitle'])
        expect(position.company_name).to eq(attributes['OrganizationName'])
      end

      it 'responds with 204 no_content status' do
        patch_request
        expect(response).to have_http_status(:no_content)
      end
    end

    context 'with data not matching referenced position' do
      let!(:position) { Fabricate(:position, reference: 'different-reference', title: 'Title', company_name: 'company') }

      it 'does not update the attributes' do
        patch_request
        position.reload
        expect(position.title).to eq('Title')
        expect(position.company_name).to eq('company')
      end

      it 'responds with 404 not found status' do
        patch_request
        expect(response).to have_http_status(:not_found)
      end
    end

    context 'with not existing position' do
      let(:position) { OpenStruct.new(id: 'asdf-fsaf-fafa') }

      it 'responds with 404 not found status' do
        patch_request
        expect(response).to have_http_status(:not_found)
      end
    end
 end

  describe 'DELETE /positions/:id' do
    let!(:position) { Fabricate(:position) }

    subject(:delete_request) do
      delete api_common_api_position_url(position.id), headers: headers
    end

    it 'unpublishes the position' do
      expect { delete_request }.to change { position.reload.published }.from(true).to(false)
    end
  end
end
