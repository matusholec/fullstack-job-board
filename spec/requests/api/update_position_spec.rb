require 'rails_helper'

RSpec.describe 'Update position', type: :request do
  let!(:position) do
    Fabricate(:position, title: 'Position title', company_name: 'Name of company')
  end
  let(:public_key) { ENV.fetch('JWT_PUBLIC_KEY') }
  let(:secret_key) { ENV.fetch('JWT_SECRET_KEY') }
  let(:token) do
    JWT.encode( { sub: public_key }, secret_key, 'HS256')
  end
  let(:headers) do
    {
      'Content-Type' => 'application/vdn.api+json',
      Authorization: "Bearer #{token}"
    }
  end
  let(:params) { {'title'=>'new title', 'company_name'=>'new company name', 'expires_on'=>'20.07.2020' } }

  subject(:update_request) do
    patch api_position_path(position), params: params.to_json, headers: headers
  end

  context 'when external api client returns true' do
    before(:each) do
      allow(::CommonApi::Client).to receive(:update_position).and_return(true)
    end

    it 'updates the position' do
      update_request
      position.reload
      expect(position.title).to eq('new title')
      expect(position.company_name).to eq('new company name')
      expect(position.expires_on.to_s).to eq('20.07.2020')
    end
  end

  context 'when external api client returns falis (e.g. fails)' do
    before(:each) do
      allow(::CommonApi::Client).to receive(:update_position).and_return(false)
    end

    it 'rollbacks the update' do
      update_request
      position.reload
      expect(position.title).to eq('Position title')
      expect(position.company_name).to eq('Name of company')
      expect(position.expires_on.to_s).to be_empty
    end
  end
end
