require 'rails_helper'

describe ::Positions::Creator, type: :service do
  let(:attributes) do
    {
      reference:    '3b571b59-93c4-4112-a5df-6c29f93fbfa2',
      title:        'Teamassistenten (m/w) für Marketing und Vertrieb',
      company_name: 'Songstar UG',
      update_uri:   'http://posting.gohiring.com/postings/3b571b59-93c4-4112-a5df-6c29f93fbfa2'
    }
  end

  subject { described_class.new(attributes) }

  it 'creates a Position record ' do
    expect { subject.call }.to change { Position.count }.from(0).to(1)
  end

  it 'assigns correct attributes' do
    position = subject.call
    expect(position.reference).to eq(attributes[:reference])
    expect(position.title).to eq(attributes[:title])
    expect(position.company_name).to eq(attributes[:company_name])
    expect(position.update_uri).to eq(attributes[:update_uri])
  end

end
