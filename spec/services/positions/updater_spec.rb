require 'rails_helper'

describe ::Positions::Updater, type: :service do
  let!(:position) { Fabricate(:position, title: 'Old title', company_name: 'Some company name') }
  let(:new_attributes) do
    {
      reference:    '3b571b59-93c4-4112-a5df-6c29f93fbfa2',
      title:        'New title',
      company_name: 'Some other company name',
    }
  end

  subject { described_class.new(position, new_attributes) }

  it 'updates a position record ' do
    subject.call
    position.reload
    expect(position.title).to eq('New title')
    expect(position.company_name).to eq('Some other company name')
  end
end
