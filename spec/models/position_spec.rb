require 'rails_helper'

RSpec.describe Position, type: :model do
  describe '#unpublish' do
    let(:position) { Fabricate(:position, published: true) }

    it 'sets published to false on position' do
      position.unpublish!
      expect(position.reload.published).to be_falsey
    end
  end
end
