require 'rails_helper'

RSpec.describe PositionSerializer, type: :serializer do
  let(:position) { Fabricate(:position) }
  subject { described_class.new(position) }

  describe 'to_h' do
    it 'serializes position into hash' do
      serialized_position = subject.to_h
      expect(serialized_position[:id]).to eq(position.id)
      expect(serialized_position[:title]).to eq(position.title)
      expect(serialized_position[:company_name]).to eq(position.company_name)
    end
  end
end
